import urllib2
import shutil
import os
import sys
import logging
import ConfigParser
import datetime
import subprocess
import io
import re
import psycopg2
import time
#import shlex

def GetAppPath():
    application_path = None
    if getattr(sys, 'frozen', False):
        application_path = os.path.dirname(os.path.abspath(sys.executable))
    elif __file__:
        application_path = os.path.dirname(os.path.abspath(__file__))
    return application_path

#config setup
config = ConfigParser.SafeConfigParser()
config.read(os.path.join(GetAppPath(),'parser.cfg'))

#url vars
urlServer = config.get("urlconfig", "server")
urlDirectory = config.get("urlconfig", "server_directory")
urlPlFile = config.get("urlconfig", "pl_file")
urlFileStart = "gfs.t"
urlFileMiddle = "z.pgrbf"
urlFileTail = ".grib2"

#time vars
timeOffset = 0
timeForecast = 0
latestDatetime = datetime.datetime(2000,1,1)
latestOffset = 0
url = None #resulting url

#wgrib2 vars
wgribDir = config.get("wgrib2config", "wgb2_dir")

#directory tree setup
if not os.path.exists(os.path.join(GetAppPath(),"log")):
    os.makedirs(os.path.join(GetAppPath(),"log"))
if not os.path.exists(os.path.join(GetAppPath(),"gribs")):
    os.makedirs(os.path.join(GetAppPath(),"gribs"))
if not os.path.exists(os.path.join(GetAppPath(),"degribbed")):
    os.makedirs(os.path.join(GetAppPath(),"degribbed"))
if not os.path.exists(os.path.join(GetAppPath(),"parsed")):
    os.makedirs(os.path.join(GetAppPath(),"parsed"))

#logging setup
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG, datefmt='%Y.%m.%d %H:%M:%S', filename=os.path.join(GetAppPath(), 'log', 'example.log'))

def OpenUrl(url):
    try:
        response = urllib2.urlopen(url)
        return response.read()
    except Exception as exc:
        logging.error(exc)
        print exc


def DownloadUrl(url, directory = None, filename=None, attempt=1):
#    def GetFileName(url,openUrl):
#        if 'Content-Disposition' in openUrl.info():
#            # If the response has Content-Disposition, try to get filename from it
#            cd = dict(map(
#                lambda x: x.strip().split('=') if '=' in x else (x.strip(),''),
#                openUrl.info()['Content-Disposition'].split(';')))
#            if 'filename' in cd:
#                filename = cd['filename'].strip("\"'")
#                if filename: return filename
#                # if no filename was found above, parse it out of the final URL.
#        return os.path.basename(urlparse.urlsplit(openUrl.url)[2])
    mAttempt = attempt
    mFilename = filename
    mUrl = url
    mDirectory = directory
    r = None
    try:
        print 'downloading from: ' + url

        r = urllib2.urlopen(urllib2.Request(url))
        application_path = GetAppPath()
        if filename is None:
            raise Exception('No filename supplied')
        if directory is not None:
            application_path = os.path.join(application_path, directory)

        filename = os.path.join(application_path, filename)
        print filename

        with open(filename, 'wb') as f:
            shutil.copyfileobj(r,f)
        logging.info("%s downloaded", filename)
        r.close()
        return 0
    except Exception as exc:
        mAttempt += 1
        logging.error(exc)
        print exc
        if str(exc) == "HTTP Error 404: data file not present":
            return 1
        if attempt <= 5:
            time.sleep(3)
            print 'restarting download in 5 seconds'
            logging.info("restarting download in 5 seconds")
            DownloadUrl(url = mUrl, directory = mDirectory, filename = mFilename, attempt=mAttempt)
        else:
            print 'exiting. could not download file'
            logging.info("exiting. could not download file")
            sys.exit(-2)

def DeGrib(filename):
    #commandLine = wgribDir +  ' -match ":[UV]GRD:[1-4].[05] " -match " mb:" ' + os.path.join(GetAppPath(), "gribs", filename) + ' -csv ' + os.path.join(GetAppPath(), "degribbed", filename) + '.txt'
    #args = shlex.split(commandLine)
    print subprocess.call(wgribDir +  ' -match ":[UV]GRD:[1-4].[05] " -match " mb:" ' + os.path.join(GetAppPath(), "gribs", filename) + ' -csv ' + os.path.join(GetAppPath(), "degribbed", filename) + '.txt', shell=True)
    #print wgribDir +  ' -match ":[UV]GRD:[1-4].[05] " -match " mb:" ' + os.path.join(GetAppPath(), "gribs", filename) + ' -csv ' + os.path.join(GetAppPath(), "degribbed", filename) + '.txt'
    #print subprocess.call([wgribDir, os.path.join(GetAppPath(), "gribs", filename) , '-match ":[UV]GRD:[1-4].[05] "' ])
    #print subprocess.Popen(args)
    logging.info("%s degribbed", filename)

def Parse(filename):
    gribStream = io.open(os.path.join(GetAppPath(), "degribbed", filename + ".txt"))
    parseStream = io.open(os.path.join(GetAppPath(), "parsed", filename + ".csv"), "w")
    for streamLine in gribStream:
        streamLine = streamLine.replace('"', '') #remove "
        streamArray = streamLine.split(',')
        streamLine = streamArray[1] + "," + ("true" if streamArray[2] == "UGRD" else "false") + "," + re.sub("\D", "", streamArray[3]) + "00" + "," + streamArray[5] + "," + str((int(streamArray[4])+180)%360) + "," + str(float(streamArray[6])) + "\n"
        parseStream.write(streamLine)

    gribStream.close()
    parseStream.close()
    logging.info("%s parsed", filename)

# def CopyToServer(filename):
#     pgConn = psycopg2.connect(config.get("dbconfig", "connection_string"))
#     cur = pgConn.cursor()
#     cur.execute("truncate app_wind_temp")
#     logging.info("wind_temp truncated")
#     parseStream = io.open(os.path.join(GetAppPath(), "parsed", filename + ".csv"))
#     cur.copy_from(parseStream, "app_wind_temp", sep=",")
#     logging.info("%s copied to server", filename)

#     cur.execute("""
#         delete from app_wind_recent
#         where datetime_active =
#             (select datetime_active
#             from app_wind_temp
#             where pressure = 30000
#             and latitude = 0
#             and longitude = 180 limit 1)""")
#     logging.info("wind_recent cleaned")

#     cur.execute("""
#         insert into app_wind_recent(
#             select wt.datetime_active, wt.pressure, wt.latitude, wt.longitude, wt.value as u, wt2.value as v
#             from app_wind_temp wt
#             inner join app_wind_temp wt2 on wt.latitude = wt2.latitude
#                 and wt.longitude = wt2.longitude
#                 and wt.pressure = wt2.pressure
#             where wt.is_u = true
#             and wt2.is_u = false);""")
#     logging.info("wind_recent updated")
#     pgConn.commit()
#     logging.info("CopyToServer comitted")
#     cur.close()
#     pgConn.close()

# def UpdateDbParameters():
#     pgConn = psycopg2.connect(config.get("dbconfig", "connection_string"))
#     cur = pgConn.cursor()
#     cur.execute("update app_parameter set value =  '" + latestDatetime.strftime("%Y-%m-%d %H:%M:%S") + "' where property_id = (select id from app_property where name = 'latest analytical wind update')")
#     logging.info("latest analytical parameter set")
#     cur.execute("""delete from app_parameter
#         where property_id = (select id from app_property where name = 'wind_recent datetimes');
#         insert into app_parameter (property_id, value)
#         select distinct on (datetime_active) (select id from app_property where name = 'wind_recent datetimes') as property_id,  datetime_active
#         from app_wind_recent""")
#     logging.info("wind_recent parameters updated")
#     cur.execute("""
#         with x as
#             (select cast(value as timestamp without time zone) as value
#             from app_parameter
#             where property_id = (select id from app_property where name = 'wind_recent datetimes'))

#         delete from app_wind_recent where datetime_active in (select * from x
#         where
#         (date_part('hour', value) != 12
#          and (select cast(value as timestamp without time zone)
#         from app_parameter where property_id = (select id from app_property where name = 'latest analytical wind update')) - value >= interval '24 hours')
#         or
#         (date_part('hour', value) = 12
#         and (select cast(value as timestamp without time zone)
#         from app_parameter where property_id = (select id from app_property where name = 'latest analytical wind update')) - value >= interval '96 hours'))""")
#     logging.info("wind_recent cleaned from outdated values")

#     cur.execute("""
#         delete from app_parameter
#         where property_id = (select id from app_property where name = 'wind_recent datetimes');""")
#     cur.execute("""
#         insert into app_parameter (property_id, value)
#         select distinct on (datetime_active) (select id from app_property where name = 'wind_recent datetimes') as property_id,  datetime_active
#         from app_wind_recent""")
#     logging.info("wind_recent parameters updated again")

#     pgConn.commit()
#     logging.info("UpdateDbParameters comitted")
#     cur.close()
#     pgConn.close()

def Vacuum():
    pgConn = psycopg2.connect(config.get("dbconfig", "connection_string"))
    cur = pgConn.cursor()
    old_isolation_level = pgConn.isolation_level
    pgConn.set_isolation_level(0)
    cur.execute("vacuum analyze app_wind_recent")
    logging.info("wind_recent vacuumed")
    pgConn.set_isolation_level(old_isolation_level)
    cur.close()
    pgConn.close()

logging.info("Process started")
logging.info("Downloading inv files")

url = urlServer + urlDirectory + urlPlFile

listPage = OpenUrl(url)
print listPage[listPage.index('filter_gfs.pl?dir=%2F')+21+4:].split('"')[0]
#print datetime.datetime.strptime(listPage[listPage.index('filter_gfs.pl?dir=%2F')+21+4:].split('"')[0],'%Y%m%d%H')
latestDatetime = datetime.datetime.strptime(listPage[listPage.index('filter_gfs.pl?dir=%2F')+21+4:].split('"')[0],'%Y%m%d%H')

#test download
logging.info("trying to test-download latest file")
url = urlServer + urlDirectory + urlPlFile + '?file=gfs.t' + latestDatetime.strftime('%H') + 'z.pgrbf00.grib2' +\
            '&lev_100_mb=on&lev_150_mb=on&lev_200_mb=on&lev_250_mb=on&lev_300_mb=on&lev_350_mb=on&lev_400_mb=on&lev_450_mb=on' +\
            '&var_UGRD=on&var_VGRD=on&leftlon=0&rightlon=360&toplat=90&bottomlat=-90&dir=%2Fgfs.' + latestDatetime.strftime('%Y%m%d%H')
if DownloadUrl(url, "gribs", 'test') != 0:
    print 'latest grib is not ready. trying previous'
    logging.info("latest grib is not ready. trying previous")
    latestDatetime -= datetime.timedelta(hours=6)

tempDateTime = latestDatetime

logging.info("+%d hours is the latest analytical grib", latestDatetime.hour)
logging.info("Downloading analytics gribs")
for i in range(4):
#    print tempDateTime.strftime('%H')
    url = urlServer + urlDirectory + urlPlFile + '?file=gfs.t' + tempDateTime.strftime('%H') + 'z.pgrbf00.grib2' + \
    '&lev_100_mb=on&lev_150_mb=on&lev_200_mb=on&lev_250_mb=on&lev_300_mb=on&lev_350_mb=on&lev_400_mb=on&lev_450_mb=on' + \
    '&var_UGRD=on&var_VGRD=on&leftlon=0&rightlon=360&toplat=90&bottomlat=-90&dir=%2Fgfs.' + tempDateTime.strftime('%Y%m%d%H')
    filename = tempDateTime.strftime('%Y%m%d%H')
    DownloadUrl(url, "gribs", filename)
    DeGrib(filename)
    Parse(filename)
    CopyToServer(filename)
    tempDateTime -= datetime.timedelta(hours=6)

forecastOffset = 0
tempDateTime = latestDatetime
for i in range(6):
    forecastOffset += 6
    tempDateTime += datetime.timedelta(hours=6)
    url = urlServer + urlDirectory + urlPlFile + '?file=gfs.t' + latestDatetime.strftime('%H') + 'z.pgrbf' + '%02d' % forecastOffset + '.grib2' +\
          '&lev_100_mb=on&lev_150_mb=on&lev_200_mb=on&lev_250_mb=on&lev_300_mb=on&lev_350_mb=on&lev_400_mb=on&lev_450_mb=on' +\
          '&var_UGRD=on&var_VGRD=on&leftlon=0&rightlon=360&toplat=90&bottomlat=-90&dir=%2Fgfs.' + latestDatetime.strftime('%Y%m%d%H')
    filename = tempDateTime.strftime('%Y%m%d%H')
    DownloadUrl(url, "gribs", filename)
    DeGrib(filename)
    Parse(filename)
    CopyToServer(filename)

#
#logging.info("Downloading forecast gribs")
#for i in [6,12,18,24,30]:
#    forecastOffset = i
#    url = urlServer + urlDirectory + urlFileStart + str(latestOffset).zfill(2) + urlFileMiddle + str(forecastOffset).zfill(2) + urlFileTail
#    filename = urlFileStart + str(latestOffset).zfill(2) + urlFileMiddle + str(forecastOffset).zfill(2) + urlFileTail
#    DownloadUrl(url, "gribs", filename)
#    DeGrib(filename)
#    Parse(filename)
#    CopyToServer(filename)

print 'removing directories'
logging.info("removing file directories")

shutil.rmtree(os.path.join(GetAppPath(), "degribbed"))
shutil.rmtree(os.path.join(GetAppPath(), "parsed"))
shutil.rmtree(os.path.join(GetAppPath(), "gribs"))

UpdateDbParameters()
Vacuum()

logging.info("Process completed")
