import urllib2
import shutil
import os
import sys
import logging
import ConfigParser
import datetime
import subprocess
import io
import re
import psycopg2

#config setup
config = ConfigParser.SafeConfigParser()
config.read('parser.cfg')

#url vars
urlServer = config.get("urlconfig", "server")
urlDirectory = config.get("urlconfig", "server_directory")
urlFileStart = "gfs.t"
urlFileMiddle = "z.pgrbf"
urlFileTail = ".grib2"

#time vars
timeOffset = 0
timeForecast = 0
latestDatetime = datetime.datetime(2000,1,1)
latestOffset = 0
url = None #resulting url

#wgrib2 vars
wgribDir = config.get("wgrib2config", "wgb2_dir")

#directory tree setup
if not os.path.exists("log"):
    os.makedirs("log")
if not os.path.exists("gribs"):
    os.makedirs("gribs")
if not os.path.exists("degribbed"):
    os.makedirs("degribbed")
if not os.path.exists("parsed"):
    os.makedirs("parsed")

#logging setup
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG, datefmt='%Y.%m.%d %H:%M:%S', filename='log\example.log')


def GetAppPath():
    application_path = None
    if getattr(sys, 'frozen', False):
        application_path = os.path.dirname(sys.executable)
    elif __file__:
        application_path = os.path.dirname(__file__)
    return application_path

def OpenUrl(url):
    response = urllib2.urlopen(url)
    return response.read()

def DownloadUrl(url, directory = None, filename=None):
#    def GetFileName(url,openUrl):
#        if 'Content-Disposition' in openUrl.info():
#            # If the response has Content-Disposition, try to get filename from it
#            cd = dict(map(
#                lambda x: x.strip().split('=') if '=' in x else (x.strip(),''),
#                openUrl.info()['Content-Disposition'].split(';')))
#            if 'filename' in cd:
#                filename = cd['filename'].strip("\"'")
#                if filename: return filename
#                # if no filename was found above, parse it out of the final URL.
#        return os.path.basename(urlparse.urlsplit(openUrl.url)[2])

    r = urllib2.urlopen(urllib2.Request(url))
    try:
        application_path = GetAppPath()
        if filename is None:
            raise Exception('No filename supplied')
        if directory is not None:
            application_path = os.path.join(application_path, directory)

        filename = os.path.join(application_path, filename)
        print filename

        with open(filename, 'wb') as f:
            shutil.copyfileobj(r,f)
        logging.info("%s downloaded", filename)
    except Exception as exc:
        logging.error(exc)
        print exc
    finally:
        r.close()

def DeGrib(filename):
    print subprocess.call(wgribDir +  ' -match ":[UV]GRD:[1-4].[05] " -match " mb:" ' + os.path.join("gribs", filename) + ' -csv ' + os.path.join("degribbed", filename) + '.txt')
    logging.info("%s degribbed", filename)

def Parse(filename):
    gribStream = io.open(os.path.join(GetAppPath(), "degribbed", filename + ".txt"))
    parseStream = io.open(os.path.join(GetAppPath(), "parsed", filename + ".csv"), "w")
    for streamLine in gribStream:
        streamLine = streamLine.replace('"', '') #remove "
        streamArray = streamLine.split(',')
        streamLine = streamArray[0] + "," + ("true" if streamArray[2] == "UGRD" else "false") + "," + re.sub("\D", "", streamArray[3]) + "00" + "," + streamArray[5] + "," + str((int(streamArray[4])+180)%360) + "," + str(float(streamArray[6])) + "\n"
        parseStream.write(streamLine)

    gribStream.close()
    parseStream.close()
    logging.info("%s parsed", filename)

def CopyToServer(filename):
    pgConn = psycopg2.connect(config.get("dbconfig", "connection_string"))
    cur = pgConn.cursor()
    cur.execute("truncate app_wind_temp")
    logging.info("wind_temp truncated")
    parseStream = io.open(os.path.join(GetAppPath(), "parsed", filename + ".csv"))
    cur.copy_from(parseStream, "app_wind_temp", sep=",")
    logging.info("%s copied to server", filename)

    cur.execute("""
        delete from app_wind_recent
        where datetime_active =
            (select datetime_active
            from app_wind_temp
            where pressure = 30000
            and latitude = 0
            and longitude = 180 limit 1)""")
    logging.info("wind_recent cleaned")

    cur.execute("""
        insert into app_wind_recent(
            select wt.datetime_active, wt.pressure, wt.latitude, wt.longitude, wt.value as u, wt2.value as v
            from app_wind_temp wt
            inner join app_wind_temp wt2 on wt.latitude = wt2.latitude
                and wt.longitude = wt2.longitude
                and wt.pressure = wt2.pressure
            where wt.is_u = true
            and wt2.is_u = false);""")
    logging.info("wind_recent updated")
    pgConn.commit()
    logging.info("CopyToServer comitted")
    cur.close()
    pgConn.close()

def UpdateDbParameters():
    pgConn = psycopg2.connect(config.get("dbconfig", "connection_string"))
    cur = pgConn.cursor()
    cur.execute("update app_parameter set value =  '" + latestDatetime.strftime("%Y-%m-%d %H:%M:%S") + "' where property = (select id from app_property where name = 'latest analytical wind update')")
    logging.info("latest analytical parameter set")
    cur.execute("""delete from app_parameter
        where property = (select id from app_property where name = 'wind_recent datetimes');
        insert into app_parameter (property, value)
        select distinct on (datetime_active) (select id from app_property where name = 'wind_recent datetimes') as property,  datetime_active
        from app_wind_recent""")
    logging.info("wind_recent parameters updated")
    cur.execute("""
        with x as
            (select cast(value as timestamp without time zone) as value
            from app_parameter
            where property = (select id from app_property where name = 'wind_recent datetimes'))

        delete from app_wind_recent where datetime_active in (select * from x
        where
        (date_part('hour', value) != 12
         and (select cast(value as timestamp without time zone)
        from app_parameter where property = (select id from app_property where name = 'latest analytical wind update')) - value >= interval '24 hours')
        or
        (date_part('hour', value) = 12
        and (select cast(value as timestamp without time zone)
        from app_parameter where property = (select id from app_property where name = 'latest analytical wind update')) - value >= interval '96 hours'))""")
    logging.info("wind_recent cleaned from outdated values")

    cur.execute("""
        delete from app_parameter
        where property = (select id from app_property where name = 'wind_recent datetimes');""")
    cur.execute("""
        insert into app_parameter (property, value)
        select distinct on (datetime_active) (select id from app_property where name = 'wind_recent datetimes') as property,  datetime_active
        from app_wind_recent""")
    logging.info("wind_recent parameters updated again")




    pgConn.commit()
    logging.info("UpdateDbParameters comitted")
    cur.close()
    pgConn.close()

def Vacuum():
    pgConn = psycopg2.connect(config.get("dbconfig", "connection_string"))
    cur = pgConn.cursor()
    old_isolation_level = pgConn.isolation_level
    pgConn.set_isolation_level(0)
    cur.execute("vacuum analyze app_wind_recent")
    logging.info("wind_recent vacuumed")
    pgConn.set_isolation_level(old_isolation_level)
    cur.close()
    pgConn.close()

logging.info("Process started")
logging.info("Downloading inv files")
for i in [0,6,12,18]:
    timeOffset = i
    url = urlServer + urlDirectory + urlFileStart + str(timeOffset).zfill(2) + urlFileMiddle + "00" + urlFileTail + ".inv"
    invFile =  OpenUrl(url)
    invDatetime = datetime.datetime(int(invFile[8:12]),int(invFile[12:14]), int(invFile[14:16]), int(invFile[16:18]))
    print invDatetime
    if invDatetime > latestDatetime:
        latestDatetime = invDatetime
        latestOffset = i

logging.info("+%s hours used", latestOffset)
logging.info("Downloading analytics gribs")
for i in [0,6,12,18]:
    timeOffset = i
    url = urlServer + urlDirectory + urlFileStart + str(timeOffset).zfill(2) + urlFileMiddle + "00" + urlFileTail
    filename = urlFileStart + str(timeOffset).zfill(2) + urlFileMiddle + "00" + urlFileTail
    #DownloadUrl(url, "gribs", filename)
    DeGrib(filename)
    Parse(filename)
    CopyToServer(filename)

logging.info("Downloading forecast gribs")
for i in [6,12,18,24,30]:
    forecastOffset = i
    url = urlServer + urlDirectory + urlFileStart + str(latestOffset).zfill(2) + urlFileMiddle + str(forecastOffset).zfill(2) + urlFileTail
    filename = urlFileStart + str(latestOffset).zfill(2) + urlFileMiddle + str(forecastOffset).zfill(2) + urlFileTail
    #    DownloadUrl(url, "gribs", filename)
    #    DeGrib(filename)
    Parse(filename)
    CopyToServer(filename)

UpdateDbParameters()
Vacuum()

logging.info("Process completed")
